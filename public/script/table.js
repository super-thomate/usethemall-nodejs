$(document).ready(function() {
	$('#pokeList').DataTable({
        // "info":     false,
        "ordering": false,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Tous"]],
        "language": {
        	"info": "Visualisation de _START_ à _END_ sur _TOTAL_ pokémons",
    		"infoEmpty": "Aucun pokémon correspondant",
    		"infoFiltered":   "(sur _MAX_ pokémons)",
        	"lengthMenu": "Voir _MENU_ pokémons",
	    	"search": "Rechercher :",
	    	"zeroRecords": "Recherche infructueuse",
	    	"paginate": {
		        "first":      "Premier",
		        "last":       "Dernier",
		        "next":       "Suivant",
		        "previous":   "Précédent"
		    }
		}
	});
	$("#loading").hide()
	$('#pokeList').fadeIn(500);


	// LE HAXX N'A PAS JOUE}
	Date.now()%3001 === 0?$('footer').append($("<small></small>").html('| <a href="/lehaxxnapasjoue">Qu\'en est-il du haxx ?</a>')):undefined;
});