$("#search").on("submit", function (event) {
	event.preventDefault();
	var generation = $("#generation").val();
	var type1 = $("#type1").val();
	var type2 = $("#type2").val();
	var rank = $("#rank").val();
	var nfe = $("#nfe").val();
	if(type1!=="all" && type1===type2) {
		// ERROR TYPE MUST BE DIFFERENT. THERE IS NO DOUBLE MONO-TYPE...
			$(".error-type").fadeIn(500);
		return ;
	}
	$(".error-type").fadeOut(500);
	var data = {
		generation,
		type1,
		type2,
		rank,
		nfe
	};
	$.ajax({
		method: "GET",
		url:"/pokemon/api/get",
		data: data
	})
	.done( function(datas) {
		doTable(datas);
	})
	.fail( function(err){
		doTable(err);
	})
});
function formatage (str) {
	return str.toLowerCase().replace(/[éèê]/g,"e");
}
function formatId (id) {
	return id < 100 ? id < 10 ? "00"+id : "0" + id : id
}
function formatRank (rank) {
	const rankArray = ["S", "A", "B", "C", "D", "E", "BAN", "MR"];
	return rankArray.includes(rank)?rank:"MR";
}
function formatNFE (nfe) {
	return nfe?"nfe":"fe";
}
function doTable (res) {
			var pokeTable = $("#pokeTable");
			const tableTemplate = $("#table-template").html();
			const pokemonTemplate = $("#pokemon-template").html();
			pokeTable.html(tableTemplate);
			var pokeList = $("#pokeList");
			$("#loading").show();
		if(res.status != 404) {
			// NO ERROR OCCURED
			var pokeListBody = $("#pokeListBody");
			res.forEach( function(pokemon) {
				var params = {
					generation: pokemon.generation,
					id: formatId(pokemon.id),
					form: pokemon.form,
					nameEn: pokemon.nameEn,
					nameFr: pokemon.nameFr,
					type1: pokemon.type1,
					type1F: formatage(pokemon.type1),
					type2: pokemon.type2,
					type2F: formatage(pokemon.type2),
					rank: formatRank(pokemon.rank),
					nfe: formatNFE(pokemon.nfe)
				}
				var html = Mustache.render(pokemonTemplate, params)
				pokeListBody.append(html);
			});
		}
		pokeList.DataTable({
	        // "info":     false,
	        "ordering": false,
	        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Tous"]],
	        "language": {
	        	"info": "Visualisation de _START_ à _END_ sur _TOTAL_ pokémons",
	    		"infoEmpty": "Aucun pokémon correspondant",
	    		"infoFiltered":   "(sur _MAX_ pokémons)",
	        	"lengthMenu": "Voir _MENU_ pokémons",
		    	"search": "Rechercher :",
		    	"zeroRecords": "Recherche infructueuse",
		    	"paginate": {
			        "first":      "Premier",
			        "last":       "Dernier",
			        "next":       "Suivant",
			        "previous":   "Précédent"
			    }
			}
		});

		$("#loading").hide()
		$('#pokeList').fadeIn(500);
}
