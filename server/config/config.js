var env= process.env.NODE_ENV || "development";

if(env === "development" || env === "test") {
	var config = require("./config.json");
	var envConfing = config[env];
	Object.keys(envConfing).forEach( (key) => {
		process.env[key] = envConfing[key];
	});
}