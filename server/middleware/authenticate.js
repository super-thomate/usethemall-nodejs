const session = require("express-session")

var {User} = require("./../models/User");

var authenticate = (req, res, next) => {
	var id = req.session.userId;
	User
		.findById(id)
		.then( (user) => {
			if(!user){
				return Promise.reject();
			}
			next();
		})
		.catch( (e) => {
			res.status(401).redirect("/login");
		});
}
module.exports = {authenticate}