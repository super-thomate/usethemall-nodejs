const session		= require("express-session");
const moment 		= require("moment");
const {User} 		= require("./../models/User");

sortPokemonById = (pokemonArray, rank) => {
	return pokemonArray.filter( (pokemon) => pokemon.rank === rank).sort( (poke1, poke2) => poke1.id - poke2.id);
}
capitalize = (str) => {
	return str.charAt(0).toUpperCase() + str.slice(1);
}
sortPokemons = (pokemons) => {
	return [].concat(sortPokemonById(pokemons, "S"), 
				sortPokemonById(pokemons, "A"),
				sortPokemonById(pokemons, "B"),
				sortPokemonById(pokemons, "C"),
				sortPokemonById(pokemons, "D"),
				sortPokemonById(pokemons, "E"),
				sortPokemonById(pokemons, "BAN"),
				sortPokemonById(pokemons, "MR"));
}
var pokemonTemplate = `
<script type="text/template" id="pokemon-template">
		 <tr>
		 	<td>
		 		<img class="sprites" src="/img/sprites/mini_{{generation}}/mini_{{generation}}_{{id}}{{form}}.png" alt="mini_{{generation}}_{{id}}{{form}}.png">
		 	</td>
			<td>
				{{id}}
			</td>
			<td>
				{{nameEn}}
			</td>
			<td>
				{{nameFr}}
			</td>
			<td>
				<a href="/pokemon/generation/{{generation}}">{{generation}}</a>
				</td>
			<td>
				<a href="/pokemon/type/{{type1}}">
					<img src="/img/types/{{type1F}}.png" alt="type1">
				</a>
				<span class="d-none">{{type1}}</span>
			{{#type2}}
				<a href="/pokemon/type/{{type2}}">
					<img src="/img/types/{{type2F}}.png" alt="type2">
				</a>
				<span class="d-none">{{type2}}</span>
			{{/type2}}
			</td>
			<td>
				<a class="rank rank-{{rank}}" href="/pokemon/rank/{{rank}}">{{rank}}</a>
			</td>
		</tr>
</script>
<script type="text/template" id="table-template">
	<table class="table table-bordered table-hover table-striped table-dark text-center" id="pokeList">
		<thead>
			<tr>
				<th>Miniature</th>
				<th>Id</th>
				<th>Nom(En)</th>
				<th>Nom(Fr)</th>
				<th>Gen</th>
				<th>Type</th>
				<th>Rang</th>
			</tr>
		</thead>
		<tbody id="pokeListBody">
		</tbody>
	</table>
</script>
`;
var displayPoke = (pokemons) => {
	// <th>Stade d'Évolution</th>
	var output = `<table class="table table-bordered table-hover table-striped table-dark text-center" id="pokeList">
		<thead>
			<tr>
				<th>Miniature</th>
				<th>Id</th>
				<th>Nom(En)</th>
				<th>Nom(Fr)</th>
				<th>Gen</th>
				<th>Type</th>
				<th>Rang</th>
			</tr>
		</thead>
		<tbody>`;
	pokemons.forEach((pokemon) => {
		var id = pokemon.id < 100 ? pokemon.id < 10 ? "00"+pokemon.id : "0" + pokemon.id : pokemon.id;
		const rankArray = ["S", "A", "B", "C", "D", "E", "BAN", "MR"];
		var rank = rankArray.includes(pokemon.rank)?pokemon.rank:"MR";
		var nfe = pokemon.nfe?"nfe":"fe";

		output += `<tr><td><img class="sprites" src="/img/sprites/mini_${pokemon.generation}/mini_${pokemon.generation}_${id}${pokemon.form}.png" alt="mini_${pokemon.generation}_${id}${pokemon.form}.png"></td>
						<td>${id}</td>
						<td>${pokemon.nameEn}</td>
						<td>${pokemon.nameFr}</td>
						<td><a href="/pokemon/generation/${pokemon.generation}">${pokemon.generation}</a></td>
						<td><a href="/pokemon/type/${pokemon.type1}"><img src="/img/types/${pokemon.type1.toLowerCase().replace(/[éèê]/g,"e")}.png" alt="type1"></a><span class="d-none">${pokemon.type1}</span>`
		if(pokemon.type2){
			output += `<a href="/pokemon/type/${pokemon.type2}"><img src="/img/types/${pokemon.type2.toLowerCase().replace(/[éèê]/g,"e")}.png" alt="type2"></a><span class="d-none">${pokemon.type2}</span></td>`;
		} else {
			output += `</td>`;
		}
		output += `
			
			<td><a class="rank rank-${rank}" href="/pokemon/rank/${rank}">${rank}</a></td></tr>`;
	});
	output += `</tbody>
	</table>`;
	return output; 
}
var displayPokeModo = (pokemons) => {
	var output = `<table class="table table-bordered table-hover table-striped table-dark text-center" id="pokeList">
		<thead>
			<tr>
				<th>Miniature</th>
				<th>Id</th>
				<th>Nom(En)</th>
				<th>Nom(Fr)</th>
				<th>Gen</th>
				<th>Type</th>
				<th>Stade d'Évolution</th>
				<th>Rang</th>
			</tr>
		</thead>
		<tbody>`;
	pokemons.forEach((pokemon) => {
		var id = pokemon.id < 100 ? pokemon.id < 10 ? "00"+pokemon.id : "0" + pokemon.id : pokemon.id;
		const rankArray = ["S", "A", "B", "C", "D", "E", "BAN", "MR"];
		var rank = rankArray.includes(pokemon.rank)?pokemon.rank:"MR";
		var nfe = pokemon.nfe?"nfe":"fe";

		output += `<tr><td><img class="sprites" src="/img/sprites/mini_${pokemon.generation}/mini_${pokemon.generation}_${id}${pokemon.form}.png" alt="mini_${pokemon.generation}_${id}${pokemon.form}.png"></td>
						<td>${id}</td>
						<td><a href="/pokemon/${pokemon.nameEn}">${pokemon.nameEn}</a></td>
						<td><a href="/pokemon/${pokemon.nameEn}">${pokemon.nameFr}</a></td>
						<td><a href="/pokemon/generation/${pokemon.generation}">${pokemon.generation}</a></td>
						<td><a href="/pokemon/type/${pokemon.type1}"><img src="/img/types/${pokemon.type1.toLowerCase().replace(/[éèê]/g,"e")}.png" alt="type1"></a><span class="d-none">${pokemon.type1}</span>`
		if(pokemon.type2){
			output += `<a href="/pokemon/type/${pokemon.type2}"><img src="/img/types/${pokemon.type2.toLowerCase().replace(/[éèê]/g,"e")}.png" alt="type2"></a><span class="d-none">${pokemon.type2}</span></td>`;
		} else {
			output += `</td>`;
		}
		output += `
		<td><img src="/img/evolution/${nfe}.png" class="nfe" alt="${nfe}"><span class="d-none">${nfe}</span></td>
		<td><a class="rank rank-${rank}" href="/pokemon/rank/${rank}">${rank}</a></td></tr>`;
	});
	output += `</tbody>
	</table>`;
	return output; 
};
function peupleSelect (selectName, optionArray, value) {
	let output;
	output = `
		<select name="${selectName}" id="${selectName}" class="custom-select form-control">\n`
			output += value===""?`<option value="" selected>Aucun</option>`:`<option value="">Aucun</option>`
			optionArray.forEach( (option) => {
				
				output += option===value?`<option value="${option}" selected>${option}</option>\n`:`<option value="${option}">${option}</option>\n`;
				
			});
			
		output +=`
		</select>
		`;
	return output;
};
function peupleSelectNFE (value) {
	let output;
	if(value) {
		output = `
		<select name="nfe" id="nfe" class="custom-select form-control">\n
			<option value="true" selected>Not Fully Evolved</option>
			<option value="false">Fully Evolved</option>
		</select>
		`;
	} else {
		output = `
		<select name="nfe" id="nfe" class="custom-select form-control">\n
			<option value="true">Not Fully Evolved</option>
			<option value="false" selected>Fully Evolved</option>
		</select>
		`;
	}
	
	return output;
};
var pokeForm = (pokemon) => {
	const typeArray = ["Acier","Combat","Dragon","Eau","Electrik","Fée","Feu","Glace","Insecte","Normal","Plante","Poison","Psy","Roche","Sol","Spectre","Ténèbres","Vol"];
	const rankArray = ["S","A", "B", "C", "D", "E", "BAN"];
	const nfeArray = ["true", "false"];
	var output;
	output = `
		<form method="post" action="/pokemon/update" class="form-group col-lg-8 offset-lg-2">
			<div class="row">
				<fieldset class="form-group col-md d-inline-block">
					<label for="nameEn">Nom anglais : </label>
					<input type="text" name="nameEn" id="nameEn" placeholder="Nom Anglais" class="form-control" required value="${pokemon.nameEn}" />
				</fieldset>
				<fieldset class="form-group col-md d-inline-block">
					<label for="nameFr">Nom français : </label>
					<input type="text" name="nameFr" id="nameFr" placeholder="Nom Français" class="form-control" required value="${pokemon.nameFr}" />
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="form-group col-md d-inline-block">
					<label for="id">Id : </label>
					<input type="number" name="id" id="id" placeholder="Id" class="form-control" required value="${pokemon.id}" min="1"/>
				</fieldset>
				<fieldset class="form-group col-md d-inline-block">
					<label for="form">Forme : </label>
					<input type="text" name="form" id="form" placeholder="M, P, T..." class="form-control" value="${pokemon.form}" title="Vide: forme normale\nM: Méga-Évolution\nMX: Méga-X\nMY: Méga-Y\nT: Totémique\nP: Primo-Résurgence"/>
				</fieldset>
				<fieldset class="form-group col-md d-inline-block">
					<label for="generation">Génération : </label>
					<input type="number" name="generation" id="generation" placeholder="Génération" class="form-control" required value="${pokemon.generation}" min="1"/>
				</fieldset>
				<fieldset class="form-group col-md d-inline-block">
					<label for="type1">Type 1 : </label>
					${peupleSelect("type1", typeArray, pokemon.type1)}
				</fieldset>
				<fieldset class="form-group col-md d-inline-block">
					<label for="type2">Type 2 : </label>
					${peupleSelect("type2", typeArray, pokemon.type2)}
				</fieldset>
				<fieldset class="form-group col-md d-inline-block">
					<label for="rank">Rang : </label>
					${peupleSelect("rank", rankArray, pokemon.rank)}
				</fieldset>
				<fieldset class="form-group col-md d-inline-block">
					<label for="nfe">NotFullyEvolved : </label>
					${peupleSelectNFE(pokemon.nfe)}
				</fieldset>
			</div>
				<input type="hidden" name="_id" value="${pokemon._id.toHexString()}"/>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Update</button>
				<button type="reset" class="btn btn-danger">Cancel</button>
			</div>
		</form>`;
	return output;
};
var random = (n) => {
	return Math.floor(Math.random()*n);
}

var displayNews = (article) => {
	var output = "";
	var name;
	var date;
	date = moment(article.time).format("DD/MM/YYYY")
	output += `
		<article class="container news">
			<h4>${article.title}</h4>
			${article.message}
			<hr />
			<small>On ${date}</small>
		</article>
	`;
	return output;
}
module.exports = {
	sortPokemonById,
	capitalize,
	sortPokemons,
	pokemonTemplate,
	displayPoke,
	displayPokeModo,
	pokeForm,
	random,
	displayNews
}