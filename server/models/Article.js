const mongoose 		= require('mongoose');
const {ObjectId} 	= require("mongodb");

var ArticleSchema = new mongoose.Schema({
	title:{
		type: String,
		required: true,
		trim: true
	},
	message:{
		type: String,
		required: true,
		trim: true
	},
	_author:{
		type: mongoose.Schema.Types.ObjectId,
		// required: true
		default: null
	},
	time:{
		type: Number,
		required: true
	}
})

var Article = mongoose.model("articles", ArticleSchema);
module.exports = {Article};