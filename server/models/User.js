const mongoose 	 = require('mongoose');
const {ObjectId} = require("mongodb");
const jwt 		 = require("jsonwebtoken");
const _ 	 	 = require("lodash");
const bcrypt	 = require("bcryptjs");

var UserSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		trim: true,
		minlength: 1,
		unique: true
	},
	password: {
		type: String,
		required: true,
		minlength: 6
	},
	admin: {
		type: Boolean,
		default: false
	}
});
UserSchema.methods.toJSON = function () {
	var user = this;
	var userObject = user.toObject();
	return _.pick(userObject, ["_id", "name"])
}

UserSchema.statics.findById = function (id) {
	var User = this;
	return User.findOne({_id: new ObjectId(id)})
		.then( (user) => {
			if(!user) {
				return Promise.reject();
			}
			return new Promise( (resolve, reject) => {
				resolve({_id:user._id, name:user.name, admin: user.admin});
			});
		});
}
UserSchema.pre('save', function (next) {
  var user = this;
  if (user.isModified('password')) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});
UserSchema.pre('findOneAndUpdate', function (next) {
  var user = this;
  if (user._update["$set"].password) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user._update["$set"].password, salt, (err, hash) => {
        user._update["$set"].password = hash;
        next();
      });
    });
  } else {
    next();
  }
});
UserSchema.statics.findByCredentials = function (name, password) {
	var User = this;

	return User
		.findOne({name})
		.then((user) => {
			if(!user){
				return Promise.reject();
			}
			return new Promise((resolve, reject) => {
				bcrypt.compare(password, user.password, (err, res) => {
					if(res){
						resolve(user);
					} else {
						reject();
					}
				});
			});
		});
};

var User = mongoose.model("users", UserSchema);

module.exports = {User}