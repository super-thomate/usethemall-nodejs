const mongoose = require('mongoose');
const {ObjectId} 	= require("mongodb");

var PokemonSchema = new mongoose.Schema({
	id: {
		type: Number,
		required: true
	},
	form: {
		type: String,
		default: "",
		trim: true
	},
	nameFr: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	nameEn: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	generation: {
		type: Number,
		required: true
	},
	type1: {
		type: String,
		required: true,
		trim: true
	},
	type2: {
		type: String
	},
	rank: {
		type: String,
		required: true,
		trim: true
	},
	nfe: {
		type: Boolean,
		default: false
	}

});

PokemonSchema.statics.findByName = function (nameEn) {
	var Pokemon = this;
	return Pokemon.findOne({
		nameEn
	});
}
PokemonSchema.statics.findByNameFr = function (nameFr) {
	var Pokemon = this;
	return Pokemon.findOne({
		nameFr
	});
}
PokemonSchema.statics.findByRank = function (rank) {
	var Pokemon = this;
	return Pokemon.find({
		rank
	});
}
PokemonSchema.statics.findByType = function (type) {
	var Pokemon = this;
	return Pokemon.find({$or:[{type1: type},{type2: type}]});
}
PokemonSchema.statics.findByGeneration = function (generation) {
	var Pokemon = this;
	return Pokemon.find({
		generation
	});
}
PokemonSchema.statics.findByPokeId = function (id) {
	var Pokemon = this;
	return Pokemon.find({
		id
	});
}
PokemonSchema.statics.findById = function (id) {
	var Pokemon = this;
	return Pokemon.findOne({
		_id: new ObjectId(id)
	})
}

var Pokemon = mongoose.model("pokemons", PokemonSchema);
module.exports = {Pokemon}