require("./config/config.js");
const path 			= require("path");
const https			= require("https");
const express 		= require("express");
const {ObjectId} 	= require("mongodb");
const _ 			= require("lodash");
const bodyParser 	= require("body-parser");
const hbs 			= require("hbs");
const fs 			= require("fs");
const url 			= require("url");
const session		= require("express-session");
const socketIO 		= require("socket.io");
const moment 		= require("moment");

var {mongoose} 		= require("./db/mongoose");
var {Pokemon} 		= require("./models/Pokemon");
var {User} 			= require("./models/User");
var {Article} 		= require("./models/Article");
var {sortPokemonById, sortPokemons, capitalize, pokemonTemplate, displayPoke, displayPokeModo, pokeForm, random, displayNews}  = require("./utils/utils");
var {authenticate} = require("./middleware/authenticate");

const publicPath = path.join(__dirname,  "/../public");
const port = process.env.PORT;
var app = express();

hbs.registerPartials(__dirname+"/../views/partials");
hbs.registerHelper("getCurrentYear", () => {
	return new Date().getFullYear();
});
hbs.registerHelper("lowerCase", (str) => {
	return str.toLowerCase().replace(/[éèê]/g,"e");
});
hbs.registerHelper("idFormat", (id) => {
	return id < 100 ? id < 10 ? "00"+id : "0" + id : id;
});
hbs.registerHelper("display", (pokemon) => {
	return pokeForm(pokemon);
});
hbs.registerHelper("listTable", (pokemons) => {
	return displayPoke(pokemons);
});
hbs.registerHelper("listModo", (pokemons) => {
	return displayPokeModo(pokemons);
});
hbs.registerHelper("isAdmin", (admin) => {
	if (admin) {
		return `<span class="badge badge-success">Administrateur</span>`;
	}
	return `<span class="badge badge-info">Modérateur</span>`;
});

app.set("view engine", "hbs");
app.use(express.static(publicPath));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(session({
	secret: 'android18',
	resave: false,
	saveUninitialized: true
}))

app.get("/", async (req, res) => {
	try {
		var admin;
		try {
			var token = req.session.token;
			const user = await User.findByToken(token);
			admin = true;
		} catch (e) {
			admin = false;
		}
		const news = await Article.find();
		var name;
		var date;
		var article;
		if(news.length !== 0) {
			article = news[news.length-1];
			date = moment(article.time).format("DD/MM/YYYY")
		}

		const pokemons = await Pokemon.find()
		res.render("home.hbs", {
			pageTitle: "Accueil",
			pokemons: sortPokemons(pokemons),
			table: true,
			logged: req.session.userId,
			pokemonTemplate,
			article,
			date
		});
	} catch (e) {
		console.log(e)
		res.status(400).send();
	}
});
app.get("/news", async (req, res) => {
	try{
		const news = await Article.find();
		if(news.length === 0) {
			throw new Error("No news found")
		}
		var output = "";
		news.forEach( (article) => {
			output += displayNews(article);
		})
		res.status(200).render("news.hbs", {
			output
		});
	} catch (e) {
		res.status(404).render("news.hbs");
	}
});
/*app.get("/rules", (req, res) => {
	res.status(200).send("WIP")
});*/
/*
app.get("/pokemon/csv", (req, res) => {
	var pokemons = JSON.parse(fs.readFileSync("./csvtojson/UseThemAll.Heroku.json"));
pokemons.forEach( async (pokemon) => {
	var body = _.pick(pokemon, ["nameEn","nfe"]);
	try {
		var poke = await Pokemon.findOneAndUpdate({nameEn: pokemon.nameEn},{$set: body}, {new:true});

	}catch(e){
		console.log(e.message)
	};
})
	res.status(200).render("temp.hbs", {json: "done"});
});
*/
app.get("/pokemon/api/get", (req, res) => {
	var generation 	= req.query.generation?req.query.generation.trim():undefined;
	var type1 		= req.query.type1?req.query.type1.trim():undefined;
	var type2		= req.query.type2?req.query.type2.trim():undefined;
	var rank 		= req.query.rank?req.query.rank.trim():undefined;
	var nfe 		= req.query.nfe?req.query.nfe.trim():undefined;
	var toFind 		= {};
	if (generation && type1 && type2 && rank && nfe) {
		generation !== "all"?toFind.generation = generation:undefined;
		rank !== "all"?toFind.rank = rank:undefined;
		// TYPES T_T
		if (type1 !== "all" && type2 === "none") {
			// MONO TYPE
			toFind.type1 = capitalize(type1);
			toFind.type2 = "";
		} else if (type1 !== "all" && type2 !== "all") {
			// DOUBLE TYPE (TYPE X & TYPE Y)
			toFind["$or"] = [{type1: capitalize(type1), type2: capitalize(type2)},{type1: capitalize(type2), type2: capitalize(type1)}];
		} else if (type1 !== "all") {
			// ALL OF TYPE X
			toFind["$or"] = [{type1: capitalize(type1)},{type2: capitalize(type1)}];
		} else if (type1 == "all" && type2 == "none") {
			// ALL MONO TYPE
			toFind.type2 = "";
		}
		nfe !== "all"?(toFind.nfe = nfe === "nfe"):undefined;
		Pokemon
		.find(toFind)
		.then( (pokemons) => {
			if (pokemons.length === 0) {
				res.status(404).send({error:"Unable to catch a pokemon", message: "No pokemon found", generation, type1, type2,rank});
			}
			pokemons = sortPokemons(pokemons);
			res.status(200).send(pokemons);
		})
		.catch( (e) => {
			res.status(400).send({error:e, message:e.message})
		});
		return ;
	}
	res.status(404).send({error:"Unable to catch a pokemon", message: "No pokemon found", generation, type1, type2,rank});
});
app.get("/pokemon/all", async (req,res) => {
	try {
		var pokemons = await Pokemon.find();
		pokemons = pokemons.map((pokemon) => {
			return {
				id: pokemon.id,
				form: pokemon.form,
				nameFr: pokemon.nameFr,
				nameEn: pokemon.nameEn,
				generation: pokemon.generation,
				type1: pokemon.type1,
				type2: pokemon.type2,
				rank: pokemon.rank,
				nfe: pokemon.nfe
			}
		})
		res.status(200).send(pokemons);
	} catch (e) {
		res.status(400).send(e);
	}
});
app.get("/pokemon/:name", authenticate, (req, res) => {
	var name = req.params.name;
	Pokemon
	.findByName(name)
	.then( (pokemon) => {
		if(!pokemon) {
			res.status(404).render("error404.hbs", {
				pageTitle: "Pokemon not Found",
				name: name,
				obj: "Pokémon",
				table: false,
				logged: req.session.userId
			});
		}
			// res.status(200).send(pokemon);
			res.status(200).render("pokemon.hbs",{
				pageTitle: `${pokemon.nameEn} (${pokemon.nameFr})`,
				pokemon: pokemon,
				logged: req.session.userId
			});
		})
	.catch( (e) => {
		res.status(400).send();
	});
});
app.post("/pokemon/update", authenticate, async (req,res) => {
	try{
		var query = _.pick(req.body,["_id"]);
		var body = _.pick(req.body,["id", "form", "nameEn", "nameFr", "generation", "type1", "type2", "rank","nfe"]);
		// console.log(body);
		var pokemon = await Pokemon.findOneAndUpdate({_id:query._id},{$set: body}, {new:true});
		// var pokemon = body;
		res.status(200).redirect(`/pokemon/${pokemon.nameEn}`);
	} catch (e) {
		console.log(e);
		res.status(400).redirect(`/pokemon/${pokemon.nameEn}`);
	}

});
app.get("/pokemon/rank/:rank", (req, res) => {
	var rank = req.params.rank;
	Pokemon
	.findByRank(rank)
	.then( (pokemons) => {
		if(pokemons.length === 0) {
			res.status(404).render("error404.hbs", {
				pageTitle: "Rank not Found",
				name: rank,
				obj: "rank",
				table: false,
				logged: req.session.userId
			});
		}
		pokemons = sortPokemonById(pokemons, rank);
		res.status(200).render("pokeList.hbs", {
			pageTitle: `Pokémons de rang ${rank}`,
			pokemons,
			table: true	,
			logged: req.session.userId
		})
	})
	.catch( (e) => {
		res.status(400).send();
	})
});
app.get("/pokemon/type/:type", (req, res) => {
	var type = capitalize(req.params.type);
	Pokemon
	.findByType(type)
	.then( (pokemons) => {
		if(pokemons.length === 0) {
			res.status(404).render("error404.hbs", {
				pageTitle: "Type not Found",
				name: type,
				obj: "type",
				table: false,
				logged: req.session.userId
			});
		}
		pokemons = sortPokemons(pokemons);
			// res.status(200).send(pokemons);
			res.status(200).render("pokeList.hbs", {
				pageTitle: `Pokémons de type ${type}`,
				pokemons,
				table: true,
				logged: req.session.userId
			})
		})
	.catch( (e) => {
		console.log(e)
		res.status(400).send();
	})
});

app.get("/pokemon/generation/:generation", (req, res) => {
	var generation = capitalize(req.params.generation);
	Pokemon
	.findByGeneration(generation)
	.then( (pokemons) => {
		if(pokemons.length === 0) {
			res.status(404).render("error404.hbs", {
				pageTitle: "Generation not Found",
				name: generation,
				obj: "generation",
				table: false,
				logged: req.session.userId
			});
		}
		pokemons = sortPokemons(pokemons);
			// res.status(200).send(pokemons);
			res.status(200).render("pokeList.hbs", {
				pageTitle: `Pokémons de la génération ${generation}`,
				pokemons,
				table: true,
				logged: req.session.userId
			});
		})
	.catch( (e) => {
		console.log(e)
		res.status(400).send();
	})
});

app.post("/pokemon", (req,res) => {
	var body = _.pick(req.body,["id", "form", "nameFr", "nameEn", "generation", "type1", "type2", "rank"]);
	var pokemon = new Pokemon(body);
	pokemon
	.save()
	.then( () => {
		res.status(200).send(pokemon);
	})
	.catch( (e) => {
		console.log(e)
		res.status(400).send();
	});
});

app.post("/pokemon/all", (req,res) => {
	for (let i = 0; i<req.body.length; i++){
		var body = _.pick(req.body[i],["id", "form", "nameFr", "nameEn", "generation", "type1", "type2", "rank"]);
		var pokemon = new Pokemon(body);
		pokemon
		.save()
		.then( () => {
			res.status(200).send(pokemon);
		})
		.catch( (e) => {
			res.status(400).send(e);
		});
	}

});
// USERS
app.get("/login", async (req,res) => {
	try {
		const user = await User.findById(req.session.userId);
		if(!user) {
			throw new Error("Not logged in")
		}
		res.redirect("/user/profile");
	} catch (e) {
		res.status(200).render("login.hbs", {
			pageTitle: "Connexion",
			table: false,
			logged: req.session.userId
		});
	}

});
app.post("/user/profile", async (req, res) => {
	try {
		const body = _.pick(req.body, ["name", "password"]);
		if (!body) {
			throw new Error("Missing credentials.");
		} else if (!body.name || !body.password)
		{
			throw new Error("Missing credentials.");
		}
		const user = await User.findByCredentials(body.name, body.password);
		req.session.userId = user._id.toHexString();
		res.status(200).redirect("/user/profile");
	}
	catch (e) {
		res.redirect("/login");
	}
});
app.get("/user/profile", authenticate,  async (req,res) => {
	try {
		const user = await User.findById(req.session.userId);
		if(!user) {
			throw new Error("No user");
		}
		let success = false;
		let error 	= false;
		req.query.update?(req.query.update==="success"?success=true:error=true):undefined;
		var pokemons = await Pokemon.find();
		res.status(200).render("profile.hbs", {
			pageTitle: `Profil de ${user.name}`,
			table: true,
			name: user.name,
			admin: user.admin,
			id: user._id.toHexString(),
			logged: req.session.userId,
			pokemons: sortPokemons(pokemons),
			success,
			error
		});
	}
	catch (e) {
		res.redirect("/login");
	}
});
app.post("/user/register", async (req,res) => {
	var body = _.pick(req.body, ["name", "password"]);
	var user = new User(body);
	try {
		await user.save();

		res.status(200).send(user);
	} catch (e) {
		res.status(400).send(e);
	}
});
app.get("/user/profile/update", authenticate, async (req, res) => {
	try {
		var id = req.query.id;
		if(id !== req.session.userId) {
			throw new Error("Cannot update profile of other users")
		}

		const user = await User.findById(req.session.userId);
		res.status(200).render("profileUpdate.hbs", {
			pageTitle : `Modifier le profil de ${user.name}`,
			name: user.name,
			logged: req.session.userId
		});
	} catch (e) {
		console.log(e)
		res.status(400).redirect("/user/profile");
	}
});
app.post("/user/update", authenticate, async (req, res) => {
	try {
		var body = _.pick(req.body, ["name", "password"]);
		var id = req.session.userId;
		var toUpdate = {};
		if(body.name) {
			toUpdate.name = body.name;
			body.password?toUpdate.password=body.password:undefined;
			const user = await User.findOneAndUpdate({_id: id}, {$set:toUpdate},{new: true});
			res.status(200).redirect("/user/profile?update=success");
			return ;
		}
		res.status(200).redirect("/user/profile?update=error");

	} catch(e) {
		console.log(e);
		res.status(400).redirect('/login');
	}
});
app.post("/user/message", authenticate, async (req, res) => {
	var body = _.pick(req.body, ["title", "message"]);
	body.time = Date.now();
	body._author = req.session.userId;
	var message = new Article(body)
	try {
		message.save();
		res.status(200).redirect("/user/profile");
	} catch (e) {
		console.log(e);
		res.status(400).redirect("/user/profile");
	}

});

app.get('/logout', authenticate, async (req, res, next) => {
	try {
		if (req.session) {
			req.session.destroy(function(err) {
				if(err) {
					next(err);
				}
			});
		}
		res.status(200).redirect('/');
	} catch (e) {
		console.log(e);
		res.status(400).redirect('/');
	}
});

// EASTER EGGS
app.get("/lehaxxnapasjoue", (req,res) => {
	res.status(200).render("lehaxxnapasjoue", {
		pageTitle: "Rappelez-vous les enfants : le haxx n'a pas joué."
	});
});

// 404 ERROR PAGE
app.get("*", (req, res) => {
	var url = req.url;
	res.status(200).render("error404.hbs", {
		pageTitle: "Page not Found",
		name: url,
		obj: "url",
		table: false,
		logged: req.session.userId
	});
});
// LISTEN
app.listen(port, () => {
	console.log(`Server is up on port ${port}`)
});
